package com.zlcode.utils;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.core.Controller;

/**
 * 返回处理工具类
 */
public class ReturnUtils {

	 /**
     * 处理返回数据
     * @param status 处理状态
     * @param message 提示语
     * @param data 返回的数据
     * @return controller 控制器
     */
	public static void show(boolean status, String message, Object data, Controller controller) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status",status);
		map.put("message",message);
		map.put("data",data);
		controller.renderJson(map);
	}
}
