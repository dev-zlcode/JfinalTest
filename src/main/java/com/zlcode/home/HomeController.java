package com.zlcode.home;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;
import com.zlcode.model.UserModel;

import java.util.List;

public class HomeController extends Controller{
	public void index() {
		renderText("Home");

		System.out.println(this.getPara("name"));
	}
	
	public void html() {
		render("view/home/index/index.html");
	}

	@Before(CacheInterceptor.class)
	@CacheName("users")
	public void sql() {
		List<UserModel> users = UserModel.dao.find("select * from users");
		
		renderJson(users);	
	}
}
