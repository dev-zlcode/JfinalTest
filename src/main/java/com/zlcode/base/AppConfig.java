package com.zlcode.base;

import com.jfinal.config.*;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.zlcode.admin.AdminController;
import com.zlcode.admin.UserController;
import com.zlcode.api.ApiController;
import com.zlcode.home.HomeController;
import com.zlcode.model.UserModel;

public class AppConfig extends JFinalConfig {
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) { 
		// 第一次使用use加载的配置将成为主配置，可以通过PropKit.get(...)直接取值 
		PropKit.use("config.txt"); 
		
		// 开发模式 
		me.setDevMode(PropKit.getBoolean("devMode"));
		
		// 设置模版引擎 freemarker
		me.setViewType(ViewType.FREE_MARKER);
		
		// 设置404 500
		me.setError404View("/view/common/error_404.html");
		me.setError500View("/view/common/error_500.html");
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/",HomeController.class);
		
		me.add("/home",HomeController.class);
		
		me.add("/admin", AdminController.class); 
		me.add("/user", UserController.class);
		
		me.add("/api", ApiController.class); 
	}
	
	/**
	 * 配置模版引擎
	 */
	public void configEngine(Engine me) {}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置Druid数据库连接池插件
		DruidPlugin dp = createDruidPlugin();
		me.add(dp);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp); 
		me.add(arp);
		arp.addMapping("users", UserModel.class);
		
		// 缓存
		me.add(new EhCachePlugin());
	}
	
	public static DruidPlugin createDruidPlugin() {
		String jdbcUrl = "jdbc:mysql://"+PropKit.get("DB_HOST")+":"+PropKit.get("DB_PORT")+"/"+PropKit.get("DB_NAME");
		return new DruidPlugin(jdbcUrl, PropKit.get("DB_USER"), PropKit.get("DB_PWD").trim());
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
		// 添加控制层全局拦截器 处理日志
		me.addGlobalActionInterceptor(new ExceptionIntoLogInterceptor());
	} 
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
}