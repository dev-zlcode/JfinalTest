package com.zlcode.admin;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/*
 * 用户是否登录校验
 */
public class LoginInterceptor implements Interceptor {

    public  void intercept(Invocation invocation){
        Controller controller=invocation.getController();
        if (controller.getSessionAttr("userId") == null) {
        	controller.redirect("/user/login");
		}else {
			invocation.invoke();
		}
    }
} 
