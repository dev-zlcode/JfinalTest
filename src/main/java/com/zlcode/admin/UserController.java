package com.zlcode.admin;

import com.jfinal.core.Controller;
import com.zlcode.utils.ReturnUtils;

public class UserController extends Controller {
	
	public void index() { 
		
		render("/view/admin/user/login.html");
	}
	
	public void register() {

		render("/view/admin/user/register.html");
	}
	
	/*
	 * 获取验证码API
	 */
	public void yzm_api() {
		renderCaptcha();
	}
	
	/*
	 * 登陆API
	 */
	public void login_api() {
		
		String username = getPara("username");
		String password = getPara("password");
		
		if (validateCaptcha("yzm")) {
			
		}else {
			ReturnUtils.show(false, "验证码错误", "123", this);
			return;
		}
		
		if (username.equals("zl") == true) {
			
		}else {
			ReturnUtils.show(false, "该用户不存在", "zl", this);
			return;
		}
		
		if (password.equals("123456") == true) {
			
		}else {
			ReturnUtils.show(false, "密码错误", "zl", this);
			return;
		}
		setSessionAttr("userId", "123");
		ReturnUtils.show(true, "成功", "zl_123456_"+getPara("yzm"), this);
	}
	
	/*
	 * 注册API
	 */
	public void regist_api() {
		
	}
}