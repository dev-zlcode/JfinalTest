package com.zlcode.admin;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

@Before(LoginInterceptor.class)
public class AdminController extends Controller {

	public void index() { 

		render("/view/admin/index/index.html");
	} 
}