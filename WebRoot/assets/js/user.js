/**
 * 前段登陆业务类
 */

var login = {
    
	
	/**
	 * 登陆
	 */	
	check:function () {
        // 获取登陆页面到用户名和密码
        var username = $('input[name="username"]').val();
        var password = $('input[name="password"]').val();
        var yzm = $('input[name="yzm"]').val();
        if(!username){
            dialog.error("用户名不能为空");
            return;
        }
        if(!password){
            dialog.error("密码不能为空");
            return;
        }
        if(!yzm){
            dialog.error("验证码不能为空");
            return;
        }

        var url = "/user/login_api";
        var data = {'username':username,'password':password,'yzm':yzm};
        // 执行异步请求 $.post
        $.post(url,data,function (result) {
            if(result.status==0){
                return dialog.error(result.message);
            }

            if(result.status==1){
                return dialog.success(result.message,'/admin');
            }
        },'JSON');
    },
    
    /** 
     * 换验证码图片 
     */ 
    yzm:function(img){
    	// 加入时间戳字符串，防止相同的链接导致图片不刷新
    	now = new Date();
        img.src = '/user/yzm_api/' + now.getTime();  
    }
}