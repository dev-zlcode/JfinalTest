# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `password` varchar(256) DEFAULT NULL COMMENT '密码',
  `username` varchar(256) DEFAULT NULL COMMENT '姓名',
  `id` int(8) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`password`, `username`, `id`)
VALUES
	('123456','张三',1),
	('123456','李四',2),
	('123456','王五',3);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;